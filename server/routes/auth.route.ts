import { Request, Response, Router } from 'express';
import * as jwt from 'jsonwebtoken';

import { User } from '../models/user.model';
import settings from '../settings';

const router: Router = Router();

router.post('/signup', async (req: Request, res: Response) => {
  console.log('POST /auth/signup');

  try {
    const user = await new User(req.body).save();
    return res.json(user.toJSON());
  } catch (err) {
    return res.status(500).json({
      message: err.message
    });
  }
});

router.post('/login', async (req: Request, res: Response) => {
  console.log('POST /auth/login');

  try {
    const user = await User.findOne({ phone: req.body.phone });
    if (!user) { return res.sendStatus(403); }

    user.comparePassword(req.body.password, (error, isMatch) => {
      if (!isMatch) { return res.sendStatus(403); }
      const token = jwt.sign({ user: user.toJSON() }, settings.SECRET_TOKEN); // , { expiresIn: 10 } seconds
      res.status(200).json({ token });
    });
  } catch (err) {
    res.status(500).json({
      message: err.message
    });
  }
});

router.get('/logout', (req: Request, res: Response) => {

});

export default router;
