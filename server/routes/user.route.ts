import { Router } from 'express';
import { invokeMap } from 'lodash';
import * as jwt from 'jsonwebtoken';

import { User } from '../models/user.model';
import settings from '../settings';

const router: Router = Router();

router.get('/', async (req, res) => {
  console.log('GET /api/users/');

  try {
    let query = {};

    if (req.query.ids) {
      query = {
        _id: {
          $in: req.query.ids
        }
      };
    }

    const users = await User.find(query);
    return res.json(invokeMap(users, 'toJSON'));
  } catch (err) {
    return res.status(500).json({
      message: err.message
    });
  }
});

router.get('/search', async (req, res) => {
  console.log('GET /api/users/search');

  try {
    const search = {
      $or: [
        {
          firstName: {
            $regex: new RegExp(req.query.q, 'gi')
          }
        },
        {
          lastName: {
            $regex: new RegExp(req.query.q, 'gi')
          }
        }
      ],
      _id: {
        $not: {
          $in: req.query.ids
        }
      }
    };

    const users = await User.find(search);
    return res.json(invokeMap(users, 'toJSON'));
  } catch (err) {
    return res.status(500).json({
      message: err.message
    });
  }
});

router.get('/:id', async (req, res) => {
  console.log('GET /api/users/:id');

  try {
    const user = await User.findById(req.params.id).populate('contacts');
    return res.json(user.toJSON());
  } catch (err) {
    return res.status(500).json({
      message: err.message
    });
  }
});

router.put('/:id', async (req, res) => {
  console.log('PUT /api/users/:id');

  try {
    const user = await User.findByIdAndUpdate(req.params.id, req.body).exec();
    const token = jwt.sign({ user: user.toJSON() }, settings.SECRET_TOKEN); // , { expiresIn: 10 } seconds
    return res.json({ token });
  } catch (err) {
    return res.status(500).json({
      message: err.message
    });
  }
});

export default router;
