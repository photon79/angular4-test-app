// Get dependencies
import * as express from 'express';
import { join } from 'path';
import * as bodyParser from 'body-parser';
import mongoose = require('mongoose');
import * as Promise from 'bluebird';

import settings from './settings';
import routes from './routes';

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(join(__dirname, '..', 'dist')));

// Set our api routes
app.use('/api', routes);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(join(__dirname, '..', 'dist/index.html'));
});

/**
 * Create Mongoose connection
 */
mongoose.Promise = Promise;

mongoose.connect(settings.MONGODB_URI)
  .then(() => {
    console.log(`Mongoose default connection open to: ${ settings.MONGODB_URI }`);
    if (process.env.DEBUG) {
      mongoose.set('debug', true);
    }
    return true;
  })
  .catch(err => {
    console.error('Could not connect to MongoDB!');
    return err;
  });

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Listen on provided port, on all network interfaces.
 */
app.listen(port, () => console.log(`API running on localhost:${port}`));
