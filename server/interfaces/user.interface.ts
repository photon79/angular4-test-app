export interface IUser {
  firstName?: string;
  lastName?: string;
  city?: string;
  email?: string;
  phone?: string;
  dob?: Date;
  password?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
