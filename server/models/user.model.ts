import * as bcrypt from 'bcryptjs';
import { Document, Schema, Model, model } from 'mongoose';
import { isEmpty } from 'lodash';

import { IUser } from '../interfaces/user.interface';

export interface IUserModel extends IUser, Document {
  fullName(): string;
  comparePassword(password: string, callback: Function): void;
}

export const UserSchema: Schema = new Schema({
  firstName: String,
  lastName: String,
  city: { type: String, default: 'Default City' },
  email: { type: String, lowercase: true, trim: true, required: false, default: '' },
  phone: { type: String, unique: true, lowercase: true, trim: true },
  dob: Date,
  password: String,
  contacts: [{ type: Schema.Types.ObjectId, ref: 'User' }],
}, {
  timestamps: true
});

UserSchema.virtual('fullName').get(function(): string {
  const fullName = this.firstName.trim() + ' ' + this.lastName.trim();
  return isEmpty(fullName.trim()) ? this.phone : fullName;
});

UserSchema.pre('save', function(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }

  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }

    bcrypt.hash(user.password, salt, function(error, hash) {
      if (error) { return next(error); }
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) { return callback(err); }
    callback(null, isMatch);
  });
};

// Omit the password when returning a user
UserSchema.set('toJSON', {
  virtuals: true,

  transform: function(doc, ret, options) {
    delete ret.password;
    return ret;
  }
});

export const User: Model<IUserModel> = model<IUserModel>('User', UserSchema);
