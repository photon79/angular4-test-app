import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

import { User } from '../models/user';

@Injectable()
export class UserResolver implements Resolve<object> {
  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (route.params.id) {
      return this.userService.getUser(route.params.id);
    }

    return new User(this.authService.currentUser);
  }
}
