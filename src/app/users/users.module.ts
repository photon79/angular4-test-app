import { Injectable, NgModule } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ServicesModule } from '../services/services.module';
import { SharedModule } from '../shared/shared.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { SearchComponent } from './search/search.component';
import { AuthGuard } from '../services/auth-guard.service';
import { EditComponent } from './edit/edit.component';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { UserResolver } from './user.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ServicesModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    RouterModule.forChild([
      {
        path: 'contacts',
        component: ListComponent,
        canActivate: [ AuthGuard ],
        data: {
          title: 'Contacts'
        }
      },
      {
        path: 'search',
        component: SearchComponent,
        canActivate: [ AuthGuard ],
        data: {
          title: 'Search friends'
        }
      },
      {
        path: 'contacts/:id',
        component: DetailComponent,
        canActivate: [ AuthGuard ],
        resolve: {
          user: UserResolver
        },
        data: {
          title: 'Contact details'
        }
      },
      {
        path: 'profile',
        component: DetailComponent,
        canActivate: [ AuthGuard ],
        resolve: {
          user: UserResolver
        },
        data: {
          title: 'Profile'
        }
      },
      {
        path: 'profile/edit',
        component: EditComponent,
        canActivate: [ AuthGuard ],
        resolve: {
          user: UserResolver
        },
        data: {
          title: 'Edit profile'
        }
      }
    ])
  ],
  declarations: [
    DetailComponent,
    ListComponent,
    SearchComponent,
    EditComponent
  ],
  exports: [
    RouterModule
  ],
  providers: [ UserResolver, Title ]
})
export class UsersModule { }
