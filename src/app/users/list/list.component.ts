import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  contacts: Array<User>;
  selectedContact: User;

  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.getContacts().subscribe(contacts => this.contacts = contacts);
  }

  selectContact(contact) {
    this.selectedContact = contact;
  }
}
