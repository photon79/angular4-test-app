import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { User } from '../../models/user';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnInit {
  @Input() user;
  @Input() type = 'horizontal';

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    if (!this.user) {
      this.user = this.route.snapshot.data.user;
    }
  }
}
