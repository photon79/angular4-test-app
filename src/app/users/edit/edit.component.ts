import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { clone } from 'lodash';

import { User } from '../../models/user';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {
  user: User;
  minDate: Date = new Date('01-01-1970');

  constructor(
    private authService: AuthService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.user = clone(this.route.snapshot.data.user);

    if (!this.user.dob) {
      this.user.dob = new Date();
    }
  }

  updateUser() {
    this.userService.editUser(this.user).subscribe(data => {
      this.user = this.authService.updateUserFromToken(data.token);
      this.router.navigate(['/profile']);
    });
  }

  back() {
    this.location.back();
  }
}
