import { Component, OnInit } from '@angular/core';
import { pull, uniq } from 'lodash';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { ToastComponent } from '../../shared/toast/toast.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {
  users: User[];
  public query: string;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    public toast: ToastComponent,
  ) { }

  ngOnInit() { }

  search() {
    this.userService.searchUsers(this.query).subscribe(users => this.users = users);
  }

  addContact(user: User): void {
    const currentUser = this.authService.currentUser;
    currentUser.contacts.push(user._id);
    currentUser.contacts = uniq(currentUser.contacts);

    this.userService.editUser(currentUser).subscribe(
      res => {
        this.authService.updateUserFromToken(res.token);
        pull(this.users, user);
        this.toast.setMessage('Contact added successfully', 'success');
      },
      error => this.toast.setMessage(error, 'danger')
    );
  }
}
