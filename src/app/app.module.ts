import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AboutComponent } from './about/about.component';
import { AppComponent } from './app.component';

import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AppRoutingModule } from './app-routing.module';
import { ServicesModule } from './services/services.module';
import { SharedModule } from './shared/shared.module';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    UsersModule,
    AppRoutingModule,
    SharedModule,
    ServicesModule
  ],
  providers: [ Title ],
  bootstrap: [AppComponent]
})
export class AppModule { }
