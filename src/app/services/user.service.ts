import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { clone, filter, indexOf } from 'lodash';

import { User } from '../models/user';
import { AuthService } from './auth.service';

@Injectable()
export class UserService {
  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(
    private http: Http,
    private authService: AuthService
  ) { }

  getUsers(): Observable<User[]> {
    return this.http.get('/api/users').map(res => res.json());
  }

  countUsers(): Observable<any> {
    return this.http.get('/api/users/count').map(res => res.json());
  }

  getUser(user_id: string): Observable<User> {
    return this.http.get(`/api/users/${user_id}`).map(res => res.json());
  }

  editUser(user): Observable<any> {
    return this.http.put(`/api/users/${user._id}`, JSON.stringify(user), this.options).map(res => res.json());
  }

  deleteUser(user): Observable<any> {
    return this.http.delete(`/api/users/${user._id}`, this.options);
  }

  searchUsers(query): Observable<User[]> {
    query = query || '';
    const userIds = clone(this.authService.currentUser.contacts);
    userIds.push(this.authService.currentUser._id);
    const ids = $.param({ ids: userIds });

    return this.http
      .get(`/api/users/search?q=${query}&${ids}`, this.options)
      .map(res => res.json());
  }

  getContacts(): Observable<User[]> {
    const contacts = this.authService.currentUser.contacts;

    if (contacts.length === 0) {
      return Observable.create(observer => {
        observer.next([]);
      });
    }

    const ids = $.param({ ids: contacts });
    return this.http.get(`/api/users/?${ids}`, this.options).map(res => res.json());
  }
}
