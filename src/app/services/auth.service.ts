import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { User } from '../models/user';

@Injectable()
export class AuthService {
  loggedIn = false;

  currentUser: User;

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  jwtHelper: JwtHelper = new JwtHelper();

  constructor(
    private http: Http,
    private router: Router
  ) {
    const token = localStorage.getItem('token');

    if (token) {
      const decodedUser = this.decodeUserFromToken(token);
      this.setCurrentUser(decodedUser);
    }
  }

  signup(user): Observable<User> {
    return this.http.post('/api/auth/signup', JSON.stringify(user), this.options).map(res => res.json());
  }

  login(credentials): Observable<User> {
    return this
      .http
      .post('/api/auth/login', JSON.stringify(credentials), this.options)
      .map((res: Response) => res.json()).map(res => {
        if (credentials.remember) {
          localStorage.setItem('token', res.token);
        }

        const decodedUser = this.decodeUserFromToken(res.token);
        this.setCurrentUser(decodedUser);
        return this.currentUser;
      });
  }

  logout(): void {
    localStorage.removeItem('token');
    this.loggedIn = false;
    this.router.navigate(['/']);
  }

  decodeUserFromToken(token) {
    return this.jwtHelper.decodeToken(token).user;
  }

  setCurrentUser(decodedUser) {
    this.loggedIn = true;
    this.currentUser = new User(decodedUser);
  }

  updateUserFromToken(token) {
    localStorage.setItem('token', token);
    const decodedUser = this.decodeUserFromToken(token);
    this.setCurrentUser(decodedUser);
    return this.currentUser;
  }
}
