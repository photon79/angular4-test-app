import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ServicesModule } from '../services/services.module';
import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: 'auth/login',
        component: LoginComponent,
        data: {
          title: 'Sign In'
        }
      },
      {
        path: 'auth/signup',
        component: SignupComponent,
        data: {
          title: 'Sign Up'
        }
      }
    ]),
    SharedModule,
    ServicesModule
  ],
  declarations: [
    LoginComponent,
    SignupComponent
  ],
  providers: [ Title ],
  exports: [
    RouterModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AuthModule { }
