import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';

import { AuthService } from '../../services/auth.service';
import { ToastComponent } from '../../shared/toast/toast.component';

function confirmationValidator(otherControl: AbstractControl): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const confirmed = control.value === otherControl.value;
    return confirmed ? null : {'confirmation': {value: control.value}};
  };
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: [ './signup.component.less' ]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  phone = new FormControl();
  password = new FormControl();
  passwordConfirmation = new FormControl();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private authService: AuthService,
  ) {
    this.phone.setValidators([
      Validators.required,
      Validators.pattern(/[+0-9]+/),
      Validators.minLength(11),
      Validators.maxLength(12)
    ]);

    this.password.setValidators([
      Validators.required,
      Validators.minLength(6),
      confirmationValidator(this.passwordConfirmation)
    ]);

    this.passwordConfirmation.setValidators([
      Validators.required,
      confirmationValidator(this.password)
    ]);
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      phone: this.phone,
      password: this.password,
      passwordConfirmation: this.passwordConfirmation
    });
  }

  setClassPhone() {
    return { 'has-error': !this.phone.pristine && !this.phone.valid };
  }

  setClassPassword() {
    return { 'has-error': !this.password.pristine && !this.password.valid };
  }

  setClassPasswordConfirmation() {
    return { 'has-error': !this.passwordConfirmation.pristine && !this.passwordConfirmation.valid };
  }

  register() {
    this.authService.signup(this.signupForm.value).subscribe(
      res => {
        this.toast.setMessage('you successfully registered!', 'success');
        this.router.navigate(['/auth/login']);
      },
      error => {
        this.toast.setMessage(error.json().message, 'danger');
      }
    );
  }
}
