import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';

import { AuthService } from '../../services/auth.service';
import { ToastComponent } from '../../shared/toast/toast.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.less' ]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  phone = new FormControl('', [
    Validators.required,
    Validators.pattern(/[+0-9]+/),
    Validators.minLength(11),
    Validators.maxLength(12)
  ]);

  password = new FormControl('', [
    Validators.required
  ]);

  remember = new FormControl();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      phone: this.phone,
      password: this.password,
      remember: this.remember
    });
  }

  setClassPhone() {
    return { 'has-error': !this.phone.pristine && !this.phone.valid };
  }

  setClassPassword() {
    return { 'has-error': !this.password.pristine && !this.password.valid };
  }

  login() {
    this.authService.login(this.loginForm.value).subscribe(
      res => this.router.navigate(['/contacts']),
      error => this.toast.setMessage(error, 'danger')
    );
  }
}
