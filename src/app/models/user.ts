import { assign } from 'lodash';

export class User {
  _id: string;
  firstName: string;
  lastName: string;
  city: string;
  email: string;
  phone: string;
  dob: Date;
  createdAt: Date;
  updatedAt: Date;
  contacts: Array<string>;

  constructor(data) {
    assign(this, data);
  }
}
